using System;
using System.Text;
using System.Timers;
using NLog;
using TServer;
using TServer.Ansi;
using static TServer.RequestParser;

namespace App.Script
{
    class SessionScript : ISessionScript
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private Session serverSession;
        private Timer timer;
        private char testChar = 'A';

        public void SessionSetUp(Session serverSession)
        {
            logger.Trace("Setting up session in session script");
            this.serverSession = serverSession;

            serverSession.OnSessionStart += OnStart;
            serverSession.AfterHandlingRequest += OnRequest;
            serverSession.OnCommandQueueHandling += (session, command) =>
            {
                if (command.CommandString == "quit")
                {
                    session.Send("Bye!");
                    session.CloseSession();
                }
            };
            serverSession.OnClientDisconnect += (session, reason) =>
            {
                timer.Stop();
                timer.Dispose();
                session.Send(new ANSIBuilder().NextLine().Text("Connection closed!!"));
            };
            timer = new Timer
            {
                Interval = 250,
                AutoReset = true
            };
            timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            logger.Trace("Timer elapsed");
            ShowCurrentChar();
        }

        private void OnStart(Session session)
        {
            session.ClearScreen();
            session.GetClientsScreenSize();
            session.Send(new ANSIBuilder()
                .AutoResetsColors()
                .TextLn(ANSI.Color.BrightCyan, "Telnet server v0.5.4b")
                .TextLn(".NET Core 2.1 | C#", 2)
                .Text(ANSI.Color.Green, "hosted at ")
                .KeepsColors()
                .BackgroundColor(ANSI.Color.Black)
                .Text(ANSI.Color.BrightBlue, "G")
                .Text(ANSI.Color.BrightRed, "o")
                .Text(ANSI.Color.BrightYellow, "o")
                .Text(ANSI.Color.BrightBlue, "g")
                .Text(ANSI.Color.BrightGreen, "l")
                .Text(ANSI.Color.BrightRed, "e")
                .AutoResetsColors()
                .Text(" ")
                .TextLn(ANSI.Color.BrightWhite, ANSI.Color.Blue, "Compute Engine", 2)
                .TextLn("Type quit<enter> to disconnect", 2));
            timer.Start();
        }

        private void OnRequest(Session session, Request request)
        {
            if (request.Type == Request.RType.ScreenSize && !session.ScreenSize.IsEmpty)
            {
                session.Send(new ANSIBuilder()
                    .TextLn($"Your terminal has {session.ScreenSize.Height} rows & {session.ScreenSize.Width} columns"));
            }
            else if (request.Type == Request.RType.ArrowKey)
            {
                bool skip(char ch) => ch == (char)10 || ch == (char)12 || ch == (char)13;
                ANSI.ArrowKey dir = (ANSI.ArrowKey)request.ParsedData;
                switch (dir)
                {
                    case ANSI.ArrowKey.Up:
                        testChar++;
                        while (skip(testChar))
                        { testChar++; }
                        break;
                    case ANSI.ArrowKey.Down:
                        testChar--;
                        while (skip(testChar))
                        { testChar--; }
                        break;
                    case ANSI.ArrowKey.Right:
                        testChar += (char)10;
                        while (skip(testChar))
                        { testChar++; }
                        break;
                    case ANSI.ArrowKey.Left:
                        testChar -= (char)10;
                        while (skip(testChar))
                        { testChar--; }
                        break;
                }
                ShowCurrentChar();
            }
        }

        private void ShowCurrentChar()
        {
            serverSession.Send(new ANSIBuilder()
                .SaveCursor()
                .SetPosition(serverSession.ScreenSize.Height - 5, serverSession.ScreenSize.Width - 30)
                .EraseLine(ANSI.Section.FromCursor)
                .Text(ANSI.Color.BrightRed, ANSI.Color.Black, $"char: {testChar} num: {(ushort)testChar} hex: {BitConverter.ToString(Encoding.Default.GetBytes(new char[] { testChar }))}")
                .RestoreCursor());
        }
    }
}
