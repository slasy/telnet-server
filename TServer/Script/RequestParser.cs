﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using NLog;
using TServer.Ansi;

namespace TServer
{
    public class RequestParser
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private Session serverSession;
        private int requestId;

        public event Action<Request> OnScreenSize;
        public event Action<Request> OnCursorPosition;
        public event Action<Request> OnInput;
        public event Action<Request> OnEOLInput;
        public event Action<Request> OnArrowKey;
        public event Action<Request> OnUnknowRequest;
        public event Action<Request> OnOtherAnsi;
        public event Action<Request> BeforeHandlingRequest;
        public event Action<Request> AfterHandlingRequest;

        public class Request
        {
            public enum RType
            {
                ScreenSize,
                CursorPosition,
                Input,
                InputWithEOL,
                ArrowKey,
                OtherAnsi,
            }

            public RType Type { get; set; }
            public object ParsedData { get; set; }
            public string Received { get; set; }
            public string ReceivedRaw { get; set; }
            public int RequestId { get; set; }
        }

        public RequestParser(Session serverSession)
        {
            this.serverSession = serverSession;
            requestId = 0;
        }

        public void HandleRequests(List<Request> requests)
        {
            foreach (Request request in requests)
            {
                logger.Debug("Request type {type}", request.Type);
                BeforeHandlingRequest?.Invoke(request);
                switch (request.Type)
                {
                    case Request.RType.ScreenSize:
                        OnScreenSize?.Invoke(request);
                        break;
                    case Request.RType.CursorPosition:
                        OnCursorPosition?.Invoke(request);
                        break;
                    case Request.RType.Input:
                        OnInput?.Invoke(request);
                        break;
                    case Request.RType.InputWithEOL:
                        OnEOLInput?.Invoke(request);
                        break;
                    case Request.RType.ArrowKey:
                        OnArrowKey?.Invoke(request);
                        break;
                    case Request.RType.OtherAnsi:
                        OnOtherAnsi?.Invoke(request);
                        break;
                    default:
                        logger.Error("Unknown request type: {type}", request.Type);
                        OnUnknowRequest?.Invoke(request);
                        break;
                }
                AfterHandlingRequest?.Invoke(request);
            }
        }

        public List<Request> GetRequests(string received)
        {
            string baseAnsiPattern = $@"({ANSI.ESC}\[(?:\d+|;)*\w)";
            string numberScrSizePattern = $@"^{ANSI.ESC}\[(\d+);(\d+)R$";
            string arrowDirectionPattern = $@"^{ANSI.ESC}\[([ABCD])$";
            bool isPosition(string str) => Regex.IsMatch(str, numberScrSizePattern);
            bool isArrow(string str) => Regex.IsMatch(str, arrowDirectionPattern);
            List<Request> requests = new List<Request>();
            foreach (var part in Regex.Split(received, baseAnsiPattern).Select((str, index) => (str, index)))
            {
                if (part.str.Length == 0)
                {
                    logger.Trace("Skipping part of request {@part}", part);
                    continue;
                }
                bool isAnsi = (part.index & 1) == 1;
                Point parsePosition(Match match) => new Point(int.Parse(match.Groups[2].Value), int.Parse(match.Groups[1].Value));
                ANSI.ArrowKey parseArrow(Match match) => (ANSI.ArrowKey)Enum.ToObject(typeof(ANSI.ArrowKey), match.Groups[1].Value.ToCharArray()[0]);
                if (isAnsi && serverSession.WaitingForScreenSize && isPosition(part.str))
                {
                    logger.Trace("screen size");
                    requests.Add(new Request
                    {
                        Type = Request.RType.ScreenSize,
                        ParsedData = new Size(parsePosition(Regex.Match(part.str, numberScrSizePattern))),
                        Received = part.str,
                        ReceivedRaw = received,
                        RequestId = requestId
                    });
                }
                else if (isAnsi && isPosition(part.str))
                {
                    logger.Trace("position");
                    requests.Add(new Request
                    {
                        Type = Request.RType.CursorPosition,
                        ParsedData = parsePosition(Regex.Match(part.str, numberScrSizePattern)),
                        Received = part.str,
                        ReceivedRaw = received,
                        RequestId = requestId
                    });
                }
                else if (isAnsi && isArrow(part.str))
                {
                    logger.Trace("arrow keys");
                    requests.Add(new Request
                    {
                        Type = Request.RType.ArrowKey,
                        ParsedData = parseArrow(Regex.Match(part.str, arrowDirectionPattern)),
                        Received = part.str,
                        ReceivedRaw = received,
                        RequestId = requestId
                    });
                }
                else if (isAnsi)
                {
                    logger.Trace("other ansi");
                    requests.Add(new Request
                    {
                        Type = Request.RType.OtherAnsi,
                        ParsedData = null,
                        Received = part.str,
                        ReceivedRaw = received,
                        RequestId = requestId
                    });
                }
                else if (!isAnsi && (part.str.EndsWith('\n') || part.str.EndsWith('\r')))
                {
                    logger.Trace("text with enter key/new line");
                    requests.Add(new Request
                    {
                        Type = Request.RType.InputWithEOL,
                        ParsedData = null,
                        Received = part.str.TrimEnd('\r', '\n'),
                        ReceivedRaw = received,
                        RequestId = requestId
                    });
                }
                else if (!isAnsi && part.str.Length > 0)
                {
                    logger.Trace("just text");
                    requests.Add(new Request
                    {
                        Type = Request.RType.Input,
                        ParsedData = null,
                        Received = part.str,
                        ReceivedRaw = received,
                        RequestId = requestId
                    });
                }
                else
                {
                    logger.Warn("Part of request was not recognised: {@part}", part.index, part.str);
                }
            }
            requestId++;
            return requests;
        }
    }
}
