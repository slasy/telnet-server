﻿using System;
using System.Net;
using CLoader;

namespace TServer
{
    internal static class ServerConfig
    {
        private static ConfigLoader config = new ConfigLoader("tserver_config.json")
            .RegisterCustomParser<IPAddress>(x => IPAddress.Parse(x));

        public static int SERVER_PORT => config.GetValue<int>("ServerPort");
        public static int SOCKET_BUFFER_SIZE => config.GetValue<int>("SocketBufferSize");
        public static IPAddress SERVER_LISTENING_IP => config.GetValue<IPAddress>("ServerListeningIP");
        public static int SHUTDOWN_WAIT_TIMEOUT_MS => config.GetValue<int>("ShutdownWaitTimeoutMS");
        public static int MAX_SECONDS_PER_SESSION => config.GetValue<int>("MaxSecondsPerSession");
    }
}
