﻿using System;
using System.Linq;
using System.Text;

namespace TServer.Ansi
{
    public static class ANSI
    {
        public const string ESC = "\x1B";
        public const string CSI = ESC + "[";

        private static Encoding _encoding;
        public static Encoding Encoding
        {
            get
            {
                if (_encoding == null)
                {
                    _encoding = new ANSIEncoding();
                }
                return _encoding;
            }
        }

        public static bool UseColors { get; set; } = true;
        public static bool UseANSI { get; set; } = true;
        public static bool NormalizeColors { get; set; } = false;
        public static bool UseAltNextLine { get; set; } = false;

        public enum Section : int
        {
            FromCursor = 0,
            ToCursor,
            Both,
            ScrollBuffer
        }

        public enum Color : int
        {
            Black = 30,
            Red,
            Green,
            Yellow,
            Blue,
            Purple,
            Cyan,
            White,
            BrightBlack = 90,
            BrightRed,
            BrightGreen,
            BrightYellow,
            BrightBlue,
            BrightPurple,
            BrightCyan,
            BrightWhite,
        }

        public enum SGR : int
        {
            Reset = 0,
            Bold,
            Underline = 4,
            SlowBlink,
            FastBlink,
            InvertColors,
            Normal = 22,
        }

        public enum ArrowKey : ushort
        {
            Up = 'A',
            Down = 'B',
            Right = 'C',
            Left = 'D',
        }

        private static string AnsiBuilder(string name, params int[] numParams) => UseANSI ? CSI + String.Join(';', numParams) + name : "";
        private static string AnsiColorBuilder(Color color) => UseColors ? AnsiBuilder("m", NormalizeColor(color)) : "";
        private static string AnsiColorBuilder(Color front, Color back) =>
            UseColors ? AnsiBuilder("m", NormalizeColor(front).ToList().Append((int)GetBackgroundColor(back)).ToArray()) : "";
        private static Color GetBackgroundColor(Color back) => (Color)NormalizeColor(10 + back).First();
        private static int[] NormalizeColor(Color color)
        {
            return !NormalizeColors ?
                new int[] { (int)color } :
                color >= Color.BrightBlack ?
                    new int[] { (int)(color - (Color.BrightBlack - Color.Black)), (int)SGR.Bold } :
                    new int[] { (int)color, (int)SGR.Normal };
        }

        public static string Up(int n = 1) => AnsiBuilder("A", n);
        public static string Down(int n = 1) => AnsiBuilder("B", n);
        public static string Right(int n = 1) => AnsiBuilder("C", n);
        public static string Left(int n = 1) => AnsiBuilder("D", n);
        public static string SetPosition(int row, int column) => AnsiBuilder("H", row, column);
        public static string GetPosition() => AnsiBuilder("n", 6);
        public static string SaveCursor() => AnsiBuilder("s");
        public static string RestoreCursor() => AnsiBuilder("u");
        public static string EraseDisplay(Section section) => AnsiBuilder("J", (int)section);
        public static string EraseLine(Section section) => AnsiBuilder("K", (int)section);
        public static string ForegroundColor(Color color) => AnsiColorBuilder(color);
        public static string BackgroundColor(Color color) => AnsiColorBuilder(GetBackgroundColor(color));
        public static string SetColor(Color front, Color back) => AnsiColorBuilder(front, back);
        public static string NormalColors() => AnsiBuilder("m", 0);
        public static string NextLine(int n = 1) =>
            UseAltNextLine ? "\r" + string.Join("", Enumerable.Range(0, n < 0 ? 1 : n).Select(i => "\n")) : AnsiBuilder("E", n);
        public static string PreviousLine(int n = 1) => AnsiBuilder("F", n);
        public static string SetColumn(int n) => AnsiBuilder("G", n);
    }

    public class ANSIEncoding : Encoding
    {
        public override int GetByteCount(char[] chars, int index, int count)
        {
            return count;
        }

        public override int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex)
        {
            chars.Skip(charIndex).Take(charCount).Select(ch => (byte)(ch & 0xff)).ToArray().CopyTo(bytes, byteIndex);
            return charCount - charIndex;
        }

        public override int GetCharCount(byte[] bytes, int index, int count)
        {
            return count;
        }

        public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
        {
            bytes.Skip(byteIndex).Take(byteCount).Select(b => (char)b).ToArray().CopyTo(chars, charIndex);
            return byteCount - byteIndex;
        }

        public override int GetMaxByteCount(int charCount)
        {
            return charCount;
        }

        public override int GetMaxCharCount(int byteCount)
        {
            return byteCount;
        }
    }
}
