﻿using System.Net.Sockets;

namespace TServer.Other
{
    static class SocketExt
    {
        public static bool IsConnected(this Socket socket, int waitTimeMs = 1)
        {
            try
            {
                return !(socket.Poll(waitTimeMs * 1000, SelectMode.SelectRead) && socket.Available == 0);
            }
            catch (SocketException)
            {
                return false;
            }
        }
    }
}
