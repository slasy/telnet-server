﻿using System.Net;
using System.Net.Sockets;
using NLog;

namespace TServer.Other
{
    static class Utils
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static IPAddress GetLocalIPAddress(bool getIpV6 = false)
        {
            IPAddress found = null;
            foreach (IPAddress ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
            {
                logger.Trace("ip: {} - {}", ip, ip.AddressFamily);
                if (ip.AddressFamily == (getIpV6 ? AddressFamily.InterNetworkV6 : AddressFamily.InterNetwork))
                {
                    found = ip;
                }
            }
            if (found != null)
            {
                return found;
            }
            logger.Error($"No network adapters with an IPv{(getIpV6 ? "6" : "4")} address in the system!");
            return IPAddress.Any;
        }
    }
}
