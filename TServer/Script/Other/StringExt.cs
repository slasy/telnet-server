﻿using System.Text;

namespace TServer.Other
{
    static class StringExt
    {
        public static byte[] GetBytes(this string str) => Encoding.UTF8.GetBytes(str);
        public static byte[] GetBytes(this string str, Encoding encoding) => encoding.GetBytes(str);

        public static string SanitizeANSI(this string str) => str.Replace("\x1B[", "^[[").Replace("\r", "\\r").Replace("\n", "\\n");

        public static string GetString(this byte[] bytes) => Encoding.UTF8.GetString(bytes);
        public static string GetString(this byte[] bytes, Encoding encoding) => encoding.GetString(bytes);
        public static string GetString(this byte[] bytes, int count) => Encoding.UTF8.GetString(bytes, 0, count);
        public static string GetString(this byte[] bytes, int count, Encoding encoding) => encoding.GetString(bytes, 0, count);
    }
}
