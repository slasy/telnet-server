﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace CLoader
{
    public sealed class ConfigLoader
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private const int WAIT_BETWEEN_TIME = 10;
        private const int MAX_ATTEMPTS = 10;

        private DateTime lastModificationTime;
        private string configFilePath;
        private Dictionary<String, object> propertyCache;
        private Dictionary<Type, CustomParser> customParsers;

        public delegate object CustomParser(string input);
        public event Action OnConfigChange;

        public ConfigLoader(string configPath)
        {
            if (!File.Exists(configPath))
            {
                throw new FileNotFoundException("Config file not found", configPath);
            }
            configFilePath = configPath;
            lastModificationTime = new DateTime();
            propertyCache = new Dictionary<string, object>();
            customParsers = new Dictionary<Type, CustomParser>();
        }

        public ConfigLoader RegisterCustomParser<OfType>(CustomParser customParser) => RegisterCustomParser(typeof(OfType), customParser);

        private ConfigLoader RegisterCustomParser<T>(T outputType, CustomParser customParser) where T : Type
        {
            if (customParsers.ContainsKey(outputType))
            {
                customParsers[outputType] = customParser;
            }
            else
            {
                customParsers.Add(outputType, customParser);
            }
            return this;
        }

        public T GetValue<T>(string propertyName)
        {
            DateTime fileChangeTime = File.GetLastWriteTimeUtc(configFilePath);
            if (lastModificationTime.CompareTo(fileChangeTime) != 0)
            {
                logger.Warn("Config file {filename} was changed, clearing cache", configFilePath);
                propertyCache.Clear();
                lastModificationTime = fileChangeTime;
                OnConfigChange?.Invoke();
            }
            if (!propertyCache.ContainsKey(propertyName))
            {
                T value = LoadProperty<T>(propertyName);
                logger.Info("Loading current state of property: {name} <{type}> [{value}]", propertyName, typeof(T), value);
                CacheProperty(propertyName, value);
            }
            return (T)propertyCache[propertyName];
        }

        public void SetValue(string propertyName, object value)
        {
            logger.Warn("Property was set from outside the config file (unit test?)");
            CacheProperty(propertyName, value);
        }

        public void ClearCache()
        {
            propertyCache.Clear();
        }

        private void CacheProperty(string propertyName, object value)
        {
            if (propertyCache.ContainsKey(propertyName))
            {
                propertyCache[propertyName] = value;
            }
            else
            {
                propertyCache.Add(propertyName, value);
            }
        }

        private T LoadProperty<T>(string propertyName)
        {
            JsonLoadSettings loadSettings = new JsonLoadSettings { CommentHandling = CommentHandling.Ignore };
            JObject config;
            try
            {
                config = JObject.Parse(LoadFile(), loadSettings);
            }
            catch (JsonReaderException e)
            {
                logger.Fatal(e, "Doesn't look like valid JSON file, fallback to default value {dafault} of type <{type}> for property: {name}", default(T), typeof(T), propertyName);
                return default(T);
            }
            return HandleTypeParsing<T>(config, propertyName);
        }

        private T HandleTypeParsing<T>(JObject config, string propertyName)
        {
            foreach (var (type, parser) in customParsers)
            {
                if (typeof(T).IsAssignableFrom(type))
                {
                    return (T)parser(GetValueFrom<string>(config, propertyName));
                }
            }
            // Handles normal JSON types: string, number, bool
            return GetValueFrom<T>(config, propertyName);
        }

        private T GetValueFrom<T>(JObject json, string name)
        {
            if (json.TryGetValue(name, out JToken value))
            {
                return value.Value<T>();
            }
            logger.Fatal("Property name {name} is not defined in config file, fallback to default value of {type}", name, typeof(T));
            return default(T);
        }

        private string LoadFile()
        {
            string configString = null;
            int attempt = 0;
            Exception err = null;
            do
            {
                try
                {
                    configString = File.ReadAllText(configFilePath);
                }
                catch (Exception e)
                {
                    err = e;
                    attempt++;
                    Thread.Sleep(WAIT_BETWEEN_TIME);
                }
            } while (configString == null && attempt < MAX_ATTEMPTS);
            if (configString == null)
            {
                logger.Fatal(err, "Can not load the config file, fallback to empty JSON");
                return new JObject().ToString();
            }
            return configString;
        }
    }
}
